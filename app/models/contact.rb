class Contact < MailForm::Base
  attribute :name,      :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :pickup, :validate => true
  attribute :dropoff, :validate => true
  attribute :nickname,  :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Cotización nueva",
      :to => "bicimensajero420@gmail.com",
      :bcc => "champion327@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end